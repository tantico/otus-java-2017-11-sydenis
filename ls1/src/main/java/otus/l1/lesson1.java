package otus.l1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.google.common.base.MoreObjects.firstNonNull;

public class lesson1 {
    public static void main(String[] args) throws IOException {
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));

        String
            a,
            b = "Не угадал!";

        a = r.readLine();
        r = null;

        if (!"123".equals(a)) a = null;

        System.out.println(firstNonNull(a, b));
    }
}
