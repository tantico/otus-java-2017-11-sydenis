package otus.dz06;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class AtmTest {
    private Atm atm;

    @BeforeEach
    void setUp() {
        atm = new Atm();
        atm.bank.clientsBank.add(1000); //add client pin=0, amount=1000
        atm.currentPin = 0;
    }

    @AfterEach
    void tearDown() {
        atm = null;
    }

    public void inMoney_simForTest(boolean accept, int... qtyByNominals) {
        int sum = 0;

        atm.cash.transactionStart();

        for (int i = 0; i < Common.nominalValues.length; i++) {
            int nominal = Common.nominalValues[i];
            sum += (qtyByNominals[i] * nominal);
        }

        if (atm.cash.acquire(qtyByNominals) && accept)
//                UI.acceptQst("Зачислить на счёт " + sum + " руб (y/n)"))
        {
            atm.bank.incAmount(atm.currentPin, sum);
            atm.bank.printAmount(atm.currentPin);
        } else
            atm.cash.transactionRollBack();
    }

    public void outMoney_simForTest(int sum) {
        atm.cash.transactionStart();

        if (!Common.isValidSum(sum)) return;

        if (atm.cash.extract(sum) &&
                atm.bank.decAmount(atm.currentPin, sum))
        {
            atm.bank.printAmount(atm.currentPin);
        } else
            atm.cash.transactionRollBack();
    }



    @Test
    void inMoney_rollback_byCash() {
        atm.cash.initForTest(990);

        int actual = 0, expected = 0;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        inMoney_simForTest(true,3,4,21,5);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void inMoney_rollback_byAccept() {
        atm.cash.initForTest(990);

        int actual = 0, expected = 0;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        inMoney_simForTest(false,3,4,5,8);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void inMoney_normal() {
        atm.cash.initForTest(990);

        int actual = 0, expected = 0;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        int [] qty ={1,2,3,4};

        inMoney_simForTest(true, qty);

        for (int i = 0; i < qty.length; i++)
            actual += (qty[i] *  atm.cash.boxes[i].getNominal());

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void outMoney_normal() {
        atm.cash.initForTest(1);
        int actual = 0, expected = 0,
        extracted = 500;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        actual -= extracted;
        outMoney_simForTest(extracted);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void outMoney_rollback_byCash() {
        atm.cash.initForTest(1);
        int actual = 0, expected = 0;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        outMoney_simForTest(10_000);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void outMoney_rollback_byBank() {
        atm.cash.initForTest(10);
        int actual = 0, expected = 0;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        outMoney_simForTest(10_000);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }

    @Test
    void outMoney_rollback_byNominal() {
        atm.cash.initForTest(1);
        int actual = 0, expected = 0,
                extracted = 504;

        for (int i = 0; i < atm.cash.boxes.length; i++)
            actual += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        outMoney_simForTest(extracted);

        for (int i = 0; i < atm.cash.boxes.length; i++)
            expected += (atm.cash.boxes[i].getQty() * atm.cash.boxes[i].getNominal());

        assertEquals(expected, actual);
    }
}