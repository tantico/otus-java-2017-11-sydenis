package otus.dz06;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MoneyBoxTest {

    private MoneyBox mb;

    @BeforeEach
    void setUp() {
        mb = new MoneyBox(Common.Nominals.n100);
        mb.incQty(100);  //add 100 banknotes
    }

    @AfterEach
    void tearDown() {
        mb = null;
    }

    @Test
    void incQty_overhead() {
        int
                source = 934,
                expected = 34;

        int actual = mb.incQty(source);
        assertEquals(expected, actual);
    }

    @Test
    void incQty_normal() {
        int
                source = 123,
                expected = 0;

        int actual = mb.incQty(source);
        assertEquals(expected, actual);
    }

    @Test
    void decQty_notFound() {
        int
                source = 123,
                expected = 23;

        int actual = mb.decQty(source);
        assertEquals(expected, actual);
    }

    @Test
    void decQty_normal() {
        int
                source = 23,
                expected = 0;

        int actual = mb.decQty(source);
        assertEquals(expected, actual);
    }

    @Test
    void decMoney_ostatok() {
        int
                source = 920,
                expected = 20;

        int actual = mb.decMoney(source);
        assertEquals(expected, actual);
    }

    @Test
    void decMoney_notFound() {
        int
                source = 1_000_000,
                expected = -1;

        int actual = mb.decMoney(source);
        assertEquals(expected, actual);
    }

    @Test
    void decMoney_normal() {
        int
                source = 900,
                expected = 0;

        int actual = mb.decMoney(source);
        assertEquals(expected, actual);
    }
}