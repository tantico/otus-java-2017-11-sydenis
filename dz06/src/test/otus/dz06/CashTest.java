package otus.dz06;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashTest {
    private Cash cash;

    @BeforeEach
    void setUp() {
        cash  = new Cash();
    }

    @AfterEach
    void tearDown() {
        cash = null;
    }

    @Test
    void acquire_overhead() {
        cash.initForTest(500);
        int[] args = {100, 600, 200, 700};

        assertFalse(cash.acquire(args));
    }

    @Test
    void acquire_normal() {
        cash.initForTest(500);
        int[] args = {100, 200, 300, 400};

        assertTrue(cash.acquire(args));
    }

    @Test
    void extract_notFound() {
        cash.initForTest(1);

        assertFalse(cash.extract(7890));
    }

    @Test
    void extract_normalComplex() {
        cash.boxes[0].incQty(0);
        cash.boxes[1].incQty(0);
        cash.boxes[2].incQty(100);
        cash.boxes[3].incQty(2);

        assertTrue(cash.extract(5100));
    }
}