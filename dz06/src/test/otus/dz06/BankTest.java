package otus.dz06;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import otus.dz06.Bank;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {
    private Bank bank;

    @BeforeEach
    void setUp() {
        bank = new Bank();
        //add client pin=0, amount=1000
        bank.clientsBank.add(1000);
    }

    @AfterEach
    void tearDown() {
        bank = null;
    }

    @Test
    void isValidPIN_badPIN() {
        assertFalse(bank.isValidPIN(555));
    }

    @Test
    void isValidPIN_goodPIN() {
        assertTrue(bank.isValidPIN(5));
    }

    @Test
    void decAmount_normal() {
        int
            expected = 900;

        bank.decAmount(0, 100);

        int actual = bank.getAccountVal(0);
        assertEquals(expected, actual);
    }

    @Test
    void decAmount_notFound_showSum() {
        int
            expected = 1000;

        bank.decAmount(0, 12345);

        int actual = bank.getAccountVal(0);
        assertEquals(expected, actual);
    }

    @Test
    void decAmount_notFound_showResult() {
        assertFalse(bank.decAmount(0, 12345));
    }

}
