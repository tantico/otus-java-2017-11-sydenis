package otus.dz06;

public class Atm {
    Bank bank = new Bank();
    Cash cash = new Cash();
    int currentPin = -1;

    public void startUI() {
        while (UI.menuMain()) {

            int tmp = UI.getPin();
            if (bank.isValidPIN(tmp)) {
                currentPin = tmp;

                while (true) {
                    tmp = UI.menuUser();
                    if (tmp == UI.menuUserExit) break;

                    switch (tmp) {
                        case UI.menuUserInput:
                            inMoney();
                            break;
                        case UI.menuUserOutput:
                            outMoney();
                            break;
                        case UI.menuUserAmount:
                            bank.printAmount(currentPin);
                            break;
                        case UI.menuUserPrintAmounts:
                            bank.printAllAmount();
                            break;
                        case UI.menuUserPrintCash:
                            cash.printCashState();
                            break;
                    }
                }
            }

        }
    }

    public void inMoney() { //в тесте подмена
        int sum = 0;
        int[] qtyByNominals = new int[Common.nominalValues.length];
        boolean badSum = false;

        cash.transactionStart();

        for (int i = 0; i < Common.nominalValues.length; i++) {
            int nominal = Common.nominalValues[i];
            qtyByNominals[i] = UI.getIntValue("Введите кол-во купюр номинала " + nominal + ": ");
            badSum = qtyByNominals[i] == -1;
            if (badSum) break;
            sum += (qtyByNominals[i] * nominal);
        }

        if (!badSum &&
            cash.acquire(qtyByNominals) &&
            UI.acceptQst("Зачислить на счёт " + sum + " руб (y/n)"))
        {
            bank.incAmount(currentPin, sum);
            bank.printAmount(currentPin);
        } else
            cash.transactionRollBack();
    }

    public void outMoney() { //в тесте подмена
        int sum = UI.getIntValue("Укажите желаемую сумму: ");

        if (!Common.isValidSum(sum)) return;

        cash.transactionStart();

        if (cash.extract(sum) &&
            bank.decAmount(currentPin, sum))
        {
            bank.printAmount(currentPin);
        } else
            cash.transactionRollBack();
    }

    public void initForTest() {
        bank.initForTest();
        cash.initForTest(0);

        System.out.println("АТМ проинициализирован тестовыми данными.");
    }
}
