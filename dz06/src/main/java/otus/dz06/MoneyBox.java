package otus.dz06;

public class MoneyBox {
    // max вместимость в купюрах
    private final int maxVolume = 1000;
    // текущее кол-во купюр
    private int qty, qtyTmp;
    // номинал хранимых купюр
    private int nominal;

    public MoneyBox(Common.Nominals nominal) {
        this.nominal = Common.nominalValues[nominal.ordinal()];
    }

    public int getQty() {
        return qty;
    }

    public int getNominal() {
        return nominal;
    }

    // возвращает кол-во непринятых купюр (превышение лимита)
    public int incQty(int qty) {
        int rejected = this.qty + qty;

        if (rejected > maxVolume) {
            rejected -= maxVolume;
            System.out.println("Переполнение: Не принято " + rejected + " купюр номиналом " + nominal +"р.");
        } else {
            rejected = 0;
            this.qty += qty;
        }

        return rejected;
    }
    // возвращает нехватающее кол-во купюр
    public int decQty(int qty){
        int notfound;

        if (this.qty < qty) {
            notfound = qty - this.qty;
        } else {
            notfound = 0;
            this.qty -= qty;
        }

        return notfound;
    }

    // возвращает сумму недовыданного остатка или -1 в случае нехватки купюр
    public int decMoney(int sum) {
        int res;

        if (decQty(sum / nominal) != 0)
            res = -1;
        else
            res = sum % nominal;

        return res;
    }

    void transactionStart () {
        qtyTmp = qty;
    }

    void transactionRollBack () {
        qty = qtyTmp;
    }

}
