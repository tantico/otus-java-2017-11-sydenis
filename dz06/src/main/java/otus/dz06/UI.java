package otus.dz06;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UI {
    static final int
        numMainMenu = 10,
        numUserMenu = 20,

        menuMainEnter = numMainMenu + 1,
        menuMainExit = numMainMenu + 0,

        menuUserInput = numUserMenu + 1,
        menuUserOutput = numUserMenu + 2,
        menuUserAmount = numUserMenu + 3,
        menuUserPrintAmounts = numUserMenu + 4,
        menuUserPrintCash = numUserMenu + 5,
        menuUserExit = numUserMenu + 0;


    public static int getPin() {
        clearScreen();
        return getIntValue("Введите ПИН: ");
    }

    public static boolean menuMain() {
        clearScreen();
        System.out.println("Система готова к работе.");
        System.out.println("Внимание! Банкомат принимает только купюры достоинством:");
        for (int n: Common.nominalValues)
            System.out.print(n + " ");
        System.out.println("руб.");
        System.out.println(menuMainEnter - numMainMenu + " - Вход");
        System.out.println(menuMainExit - numMainMenu + " - Выход");

        return getMenuItem(numMainMenu) == menuMainEnter;
    }

    public static int menuUser() {
        clearScreen();
        System.out.println(menuUserInput - numUserMenu + " - Ввод наличных");
        System.out.println(menuUserOutput - numUserMenu + " - Получение наличных");
        System.out.println(menuUserAmount - numUserMenu + " - Состояние счёта");
        System.out.println(menuUserPrintAmounts - numUserMenu + " - Печать всех счетов (для теста)");
        System.out.println(menuUserPrintCash - numUserMenu + " - Наличие купюр (для теста)");
        System.out.println(menuUserExit - numUserMenu + " - Смена пользователя");

        return getMenuItem(numUserMenu);
    }

    private static void clearScreen() {
        for (int i = 0; i < 1; i++) System.out.println();
    }

    public static String getAnswer(String prompt) {
        System.out.print(prompt);

        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            prompt = br.readLine();
        }
        catch (IOException io) {
            System.out.println(io.getMessage());
        }

        return prompt;
    }

    public static boolean acceptQst(String prompt) {
        prompt = getAnswer(prompt);

        return
            "y".equals(prompt) ||
            "Y".equals(prompt);
    }

    public static int getIntValue(String prompt) {
        int val = -1;
        prompt = getAnswer(prompt);

        try {
            val = Integer.parseInt(prompt);
        } catch (NumberFormatException n) {
            System.out.println("Число введено не верно. Попробуйте ещё раз.");
        }

        return val;
    }

    public static int getMenuItem(int menuNum){
        return menuNum + getIntValue("Ваш выбор: ");
    }
}
