package otus.dz06;

public class Main {
    public static void main(String[] args) {
        Atm atm = new Atm();

        atm.initForTest();
        atm.startUI();
    }
}
