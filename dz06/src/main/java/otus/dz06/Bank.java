package otus.dz06;

import java.util.ArrayList;
import java.util.Random;

public class Bank {
    private final int
            maxClientsCount = 10;

    // Индекс - это pin клиента, Integer - сумма на счету (int потому что кратна купюрам)
    ArrayList<Integer> clientsBank = new ArrayList<>();

    public boolean isValidPIN(int pin) {
        return (pin >= 0 && pin < maxClientsCount);
    }

    public int getAccountVal(int pin) {
        return clientsBank.get(pin);
    }

    void incAmount(int pin, int sum) {
        int oldSum = getAccountVal(pin);
        clientsBank.set(pin, oldSum + sum);
    }

    boolean decAmount(int pin, int sum) {
        int oldSum = getAccountVal(pin);

        boolean passed = (oldSum >= sum);

        if (passed)
            clientsBank.set(pin, oldSum - sum);
        else
            System.out.println("Не хватает средств на счёте");

        return passed;
    }

    public void printAmount(int pin) {
        System.out.println("Остаток на счёте: " + getAccountVal(pin) + "руб.");
    }

    public void printAllAmount() {
        for (int i = 0; i < maxClientsCount; i++)
            System.out.println("ПИН " + i + " на счету: " + clientsBank.get(i) + " р.");
    }

    public void initForTest() {
        Random r = new Random();

        for (int i = 0; i < maxClientsCount; i++)
            clientsBank.add(r.nextInt(10000000));
    }
}
