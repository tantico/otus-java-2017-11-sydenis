package otus.dz06;

public class Common {
    // важно размещение номиналов по убыванию
    // используется в atm.isValidSum, boxes constructor и boxes.extract и др.
    public enum Nominals {n5000, n1000, n500, n100 };

    final static int [] nominalValues = {5000, 1000, 500, 100};

    public static boolean isValidSum(int sum) {
        boolean res = sum % nominalValues[nominalValues.length - 1] == 0;

        if (!res)
            System.out.println("Банкомат оперирует купюрами не менее " + nominalValues[nominalValues.length - 1] + "р. Измените сумму.");

        return res;
    }
}
