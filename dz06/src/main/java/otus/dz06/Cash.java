package otus.dz06;

import java.util.Random;

public class Cash {
    MoneyBox[] boxes;

    public Cash() {
        boxes = new MoneyBox[Common.nominalValues.length];

        for (Common.Nominals n: Common.Nominals.values())
            boxes[n.ordinal()] = new MoneyBox(n);
    }

    boolean acquire(int... qty) {
        boolean passed = true;

        for (int i = 0; i < qty.length; i++) {
            passed = passed && (boxes[i].incQty(qty[i]) == 0);
        }

        return passed;
    }

    boolean extract(int sum)  {
        int ostatok = -1;

        for (int i = 0; i < boxes.length; i++) {
            ostatok = boxes[i].decMoney(sum);

            if (ostatok == 0)
                break;
            else
                if (ostatok > 0)
                    sum = ostatok;
        }

        boolean passed = (ostatok == 0);

        if (!passed)
            System.out.println("Операция отклонена: Не хватает купюр.");

        return passed;
    }

    void transactionStart() {
        for (MoneyBox b: boxes)
            b.transactionStart();
    }

    void transactionRollBack () {
        for (MoneyBox b: boxes)
            b.transactionRollBack();
    }

    public void printCashState() {
        System.out.println("Состояние накопителей купюр:");

        for (MoneyBox b: boxes)
            System.out.println("Номиналом " + b.getNominal()   + " в наличии " + b.getQty() + " купюр");
    }

    public void initForTest(int qty) {
        Random r = new Random();

        for (int i = 0; i < boxes.length; i++)
            boxes[i].incQty(qty != 0 ? qty : r.nextInt(1000));
    }
}
