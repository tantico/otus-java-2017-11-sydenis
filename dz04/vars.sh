#!/usr/bin/env bash
FNAME=$(basename $0 .sh)
APPLOG=log_app/$FNAME".app"
GCDIR=log_gc/$FNAME".gc"
MEMORY="-Xms512m -Xmx512m -XX:MaxMetaspaceSize=256m"
GC_LOG=" -verbose:gc -Xloggc:"$GCDIR" -XX:+PrintGCDateStamps -XX:+PrintGCDetails"

