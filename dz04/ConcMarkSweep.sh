#!/usr/bin/env bash
source vars.sh
export APPLOG MEMORY GC_LOG

GC="-XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 -XX:+ScavengeBeforeFullGC -XX:+CMSScavengeBeforeRemark -XX:+UseParNewGC"
java $MEMORY $GC $GC_LOG -jar target/dz04-1.0-SNAPSHOT.jar > $APPLOG

