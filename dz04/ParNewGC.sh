#!/usr/bin/env bash
source vars.sh
export APPLOG MEMORY GC_LOG

GC="-XX:+UseParNewGC"
java $MEMORY $GC $GC_LOG -jar target/dz04-1.0-SNAPSHOT.jar > $APPLOG
