package otus.dz04;

public class Main {
    public static void main(String[] args) {
        int
            arSize = 10_599_999,
            stepLimit = 50,//Integer.parseInt(args[0]),
            step = 0;

        System.out.println("Начинаем программу из " + stepLimit + " шагов: заполнение массивов по " + arSize + " элементов");

        Object [] sArray;
        long
            startTime = 0,
            stepTime;


        while (step < stepLimit) {
            stepTime = System.currentTimeMillis();
            step++;
            sArray = new Object[arSize];

            for (int i = 0; i < arSize; i++)
                sArray[i] = new String(new char[0]);

            stepTime = System.currentTimeMillis() - stepTime;
            startTime += stepTime;
            System.out.println("Шаг " + step + " выполнен за: " + stepTime + " ms. В полёте " + startTime + " ms.");
        }

        System.out.println("Штатное окончание");
    }
}
