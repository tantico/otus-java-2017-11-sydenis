package otus.dz05;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Tester<T> {
    private List<Method>
        mBefore,
        mTest,
        mAfter;

    public Class<T> testerClass;

    private void fillTMethods() {
        mBefore = new ArrayList<>();
        mTest = new ArrayList<>();
        mAfter = new ArrayList<>();

        for (Method m: testerClass.getMethods()) {
            if (m.isAnnotationPresent(Anotations.Before.class))
                mBefore.add(m);
            if (m.isAnnotationPresent(Anotations.Test.class))
                mTest.add(m);
            if (m.isAnnotationPresent(Anotations.After.class))
                mAfter.add(m);
        }
    }

    private void runMethod(Method m) {
        try {
            T currentTester = testerClass.newInstance();

            mBefore.get(0).invoke(currentTester, null);
            m.invoke(currentTester, null);
            mAfter.get(0).invoke(currentTester, null);
        }
        catch (InstantiationException i) {
            new RuntimeException(i.getCause());
        }
        catch (IllegalAccessException e1) {
            new RuntimeException(m.getName() + " : " + e1.getCause());
        }
        catch (InvocationTargetException e2) {
            System.out.println(m.getName() + ": тест не пройден");
        }
    }

    public void runTests (){
        for (Method m: mTest)
            runMethod(m);
    }

    public static void main(String[] args) {
        Tester<TestExcavator> t = new Tester();

        t.testerClass = TestExcavator.class;
        t.fillTMethods();

        t.runTests();
    }
}
