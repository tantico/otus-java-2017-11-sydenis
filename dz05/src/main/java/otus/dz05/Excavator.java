package otus.dz05;

public class Excavator {

    // объём кучи/ямы земли
    int terraVolume = 0;
    // объём ковша
    final private int kovshVolume = 50;
    // расход топлива не перемещение одного ковша
    final private double rashodDizelNaKovsh = 0.1;
    // объём бензобака
    private double dizelBak;
    // текущий уровень топлива
    public double getDizelBak() {
        return dizelBak;
    }
    // заправка бульдозера топливом
    public void setDizelBak(double dizelBak) {
        this.dizelBak = dizelBak;
    }
    // потребление топлива из бака одним перемещением ковша
    public void rashodDizel1Kovsh() {
        dizelBak =- rashodDizelNaKovsh;
    }
    // текущее состояние кучи/ямы земли
    public int getTerraVolume() {
        return terraVolume;
    }
    // выкопать заданное кол-во ковшей
    public int otkopat(int kovshCount) {
        int qty = 0;

        for (int i = 0; i < kovshCount; i++) {
            rashodDizel1Kovsh();
            qty = +kovshVolume;
        }

        terraVolume =+ qty;
        return qty;
    }
    // зарыть яму ранее извлечённой землёй
    public int zakopat() {
        int kovshCount = 0;

        while (terraVolume >= 0) {
            rashodDizel1Kovsh();
            kovshCount++;
            terraVolume =- kovshVolume;
        }

        return kovshCount;
    }
}
