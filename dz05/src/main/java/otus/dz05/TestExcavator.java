package otus.dz05;

public class TestExcavator {

    Excavator excavator;

    @Anotations.Before
    public void init() {
      excavator = new Excavator();
    }

    @Anotations.Test
    public void setBakPositiveValue(){
        excavator.setDizelBak(-1);

        if (excavator.getDizelBak() < 0)
            throw new RuntimeException("Нельзя заправить отрицательное кол-во");
    }

    @Anotations.Test
    public void dontWorkWithoutFuel(){
        excavator.rashodDizel1Kovsh();

        if (excavator.getDizelBak() < 0)
            throw new RuntimeException("Не хватает горючего в баке");
    }

    @Anotations.Test
    public void dontWorkWithoutGround(){
        excavator.zakopat();

        if (excavator.terraVolume < 0)
            throw new RuntimeException("Нет земли для закапывания ямы");
    }

    @Anotations.After
    public void fin() {
        excavator = null;
    }

}
