package otus.dz11.cache;

import otus.dz11.User;
import otus.dz11.db.DBService;
import otus.dz11.db.UserDAO;
import otus.dz11.db.UserDAOimpl;

import java.sql.SQLException;

public class UserDAOcached implements UserDAO{

    private UserDAOimpl db;
    private CacheService cache;

    public UserDAOcached(int maxSize, int lifeTime) {
        db = new UserDAOimpl();
        cache = new CacheService(maxSize, lifeTime);
    }

    @Override
    public void connectDB(DBService ds) {
        db.connectDB(ds);
    }

    @Override
    public void disconnectDB() throws SQLException {
        cache.stopCaching();
        db.disconnectDB();
    }

    @Override
    public long insert(User user) throws SQLException {
        long id = db.insert(user);

        if (id != -1)
            cache.putItem(user);

        return id;
    }

    @Override
    public User load(long id) throws SQLException {
        User user = cache.getItem(id);

        if (user == null)
            user = db.load(id);

        return user;
    }
}
