package otus.dz11.db;

import otus.dz11.User;

import java.sql.SQLException;

public interface UserDAO {
    void connectDB(DBService ds);
    void disconnectDB() throws SQLException;
    long insert(User user) throws SQLException;
    User load(long id) throws SQLException;
}
