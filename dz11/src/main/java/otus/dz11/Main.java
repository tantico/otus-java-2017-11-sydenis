package otus.dz11;

import otus.dz11.cache.UserDAOcached;
import otus.dz11.db.DBService;
import otus.dz11.db.UserDAO;
import otus.dz11.db.UserDAOimpl;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, InterruptedException {
        final int MAX_SIZE = 10;
        final int LIFE_TIME = 700;

        DBService ds = new DBService();

        UserDAO ud = new UserDAOcached(MAX_SIZE, LIFE_TIME);
        ud.connectDB(ds);

        long existingUser = 2;

        User user = ud.load(existingUser);
        System.out.println("existing:");
        System.out.println(user.toString());

        user = new User();
        user.setName("Donald Trump");
        user.setAge(77);
        existingUser = ud.insert(user);

        user = null;
        System.out.println();
        System.out.println("just added:");
        System.out.println(ud.load(existingUser).toString());

        System.out.println();
        System.out.println("waiting for autocleaning cache...");

        for (int i = 0; i < 3; i++) {
            Thread.sleep(1000);
            System.out.println(ud.load(existingUser).toString());
        }

        ud.disconnectDB();
    }
}
