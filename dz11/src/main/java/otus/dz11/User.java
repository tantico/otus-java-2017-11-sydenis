package otus.dz11;

public class User {
    private long id;
    private String name;
    private int age;

    @Override
    public String toString() {
        return "User id = " + getId() + ", name = " + getName() + ", age = " + getAge();
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
