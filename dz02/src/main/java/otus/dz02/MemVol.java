package otus.dz02;

public class MemVol {
    private MeasureStand ms;

    public MemVol (int testArraySize) {
        ms = new MeasureStand(testArraySize);
    }

    void testMem(String objectName) {
        System.out.println(objectName + " size is " + ms.testSize(objectName));
    }

    public static void main(String[] args) {
        MemVol mv = new MemVol(2_000_000);

        mv.testMem("Object");
        mv.testMem("String");
        mv.testMem("String(pool)");
        mv.testMem("CustomClass");
        mv.testMem("Refs");
    }
}
