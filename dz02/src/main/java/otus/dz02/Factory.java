package otus.dz02;

public class Factory {

    static Object newItem(String objName) {
        Object res;

        switch (objName) {
            case "Object":        res = new Object();
                break;
            case "String":        res = new String(new char[0]);
                break;
            case "String(pool)":  res = new String("");
                break;
            case "CustomClass":   res = new CustomClass(8);
                break;
            case "Refs":          res = new Object();  //don't count
                break;
            default:              res = null;
                break;
        }

        return res;
    }
}
