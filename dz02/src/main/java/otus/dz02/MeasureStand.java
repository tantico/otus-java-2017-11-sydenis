package otus.dz02;

public class MeasureStand {
    private Runtime runtime = Runtime.getRuntime();
    private int testArraySize;
    private Object[] testArray;
    private long refSize;

    MeasureStand(int testArraySize) {
        CleanMem();

        this.testArraySize = testArraySize;
        testArray = new Object[testArraySize];

        CleanMem();
        refSize = (runtime.totalMemory() - runtime.freeMemory()) / testArraySize;
    }

    public long testSize (String objectName) {
        if (Factory.newItem(objectName) == null)
            return 0;

        if ("Refs".equals(objectName))
            return refSize;

        for (int i = 0; i < testArraySize; i++)
            testArray[i] = Factory.newItem(objectName);

        CleanMem();

        return (runtime.totalMemory() - runtime.freeMemory()) / testArraySize - refSize;
    }


    private void CleanMem () {
        System.gc();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
