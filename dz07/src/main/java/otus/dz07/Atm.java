package otus.dz07;

import java.util.HashMap;
import java.util.Random;

public class Atm {
    MoneyBox[] boxes;

    public Atm() {
        boxes = new MoneyBox[Nominals.length()];

        for (Nominals n: Nominals.values())
            boxes[n.ordinal()] = new MoneyBox(n);
    }

    /**
     * Приём купюр
     * @param qty[] - Закладываемое кол-во купюр. Учитывает порядок в Nominals. Если нужно пропустить номинал в середине списка - нужен 0
     * @return возвращает true в случае успеха операции
     */
    boolean acquire(int... qty) {
        boolean passed = true;

        for (int i = 0; i < qty.length; i++) {
            passed = passed && (boxes[i].incQty(qty[i]) == 0);
        }

        return passed;
    }

    boolean extract(int sum)  {
        int ostatok = -1;

        for (int i = 0; i < boxes.length; i++) {
            ostatok = boxes[i].decMoney(sum);

            if (ostatok == 0)
                break;
            else
                sum = ostatok;
        }

        boolean passed = (ostatok == 0);

        if (!passed)
            System.out.println("Операция отклонена: Не хватает купюр.");

        return passed;
    }

    public HashMap<Nominals, Integer> getState() {
        HashMap<Nominals, Integer> res = new HashMap<>();

        for (MoneyBox b: boxes)
            res.put(b.getNominal(), b.getQty());

        return res;
    }

    public void saveRegistration () {
        for (MoneyBox b: boxes)
            b.saveRegistration();
    }

    public void resetToRegistration () {
        for (MoneyBox b: boxes)
            b.resetToRegistration();
    }

    public void initForTest(int qty) {
        Random r = new Random();

        for (int i = 0; i < boxes.length; i++)
            boxes[i].incQty(qty != 0 ? qty : r.nextInt(1000));
    }
}
