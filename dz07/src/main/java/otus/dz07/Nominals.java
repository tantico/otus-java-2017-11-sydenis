package otus.dz07;

/** важно размещение номиналов по убыванию */
public enum Nominals {
        n5000(5000),
        n1000(1000),
        n500(500),
        n100(100);

        int value;

        Nominals(int value) {
            this.value = value;
        }

        int getValue() {
            return value;
        }

        static int min() {
            return n100.getValue();
        }

        static int length() {
            return n100.ordinal() + 1;
        }
}
