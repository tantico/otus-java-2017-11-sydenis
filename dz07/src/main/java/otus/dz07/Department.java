package otus.dz07;

import java.util.ArrayList;
import java.util.HashMap;

public class Department {

    private class AtmInfo {
        Atm atm;
        HashMap<Nominals, Integer> state;

        public AtmInfo(Atm atm) {
            this.atm = atm;
            this.state = atm.getState();
            atm.saveRegistration();
        }
    }

    private ArrayList<AtmInfo> atmList = new ArrayList<>();

    public void registerAtm(Atm atm) {
        boolean notRegistered = true;

        for (AtmInfo ai: atmList) {
            notRegistered = !ai.atm.equals(atm);
            if (!notRegistered)
                break;
        }

        if (notRegistered) {
            atm.saveRegistration();
            atmList.add(new AtmInfo(atm));
        }
    }

    public void unregisterAtm(Atm atm) {
        for (AtmInfo ai: atmList)
          if (ai.atm.equals(atm)) {
              atmList.remove(ai);
              break;
          }
    }

    public void getStates() {
        for (AtmInfo ai: atmList)
            ai.state = ai.atm.getState();
    }

    public void reInitialaize() {
        for (AtmInfo ai: atmList)
            ai.atm.resetToRegistration();

        getStates();
    }

    public void printStates() {
        getStates();

        for (AtmInfo ai : atmList) {
            System.out.println("ATM code: " + ai.atm.toString());

            for (HashMap.Entry<Nominals, Integer> e : ai.state.entrySet())
                System.out.println("nom: " + e.getKey().toString() + " length: " + e.getValue());

            System.out.println();
        }
    }
}
