package otus.dz07;

public class MoneyBox {
    /** max вместимость в купюрах */
    private final int maxVolume = 1000;
    /** текущее кол-во купюр */
    private int qty, qtyOnRegistration;
    /** номинал хранимых купюр */
    private Nominals nominal;

    public MoneyBox(Nominals nominal) {
        this.nominal = nominal;
    }

    public int getQty() {
        return qty;
    }

    public Nominals getNominal() {
        return nominal;
    }

    /**
     * Приём купюр
     * @param qty - закладываемое кол-во купюр
     * @return возвращает кол-во непринятых купюр (превышение лимита)
     */
    public int incQty(int qty) {
        int rejected = this.qty + qty;

        if (rejected > maxVolume) {
            rejected -= maxVolume;
            System.out.println("Переполнение: Не принято " + rejected + " купюр номиналом " + nominal +"р.");
        } else {
            rejected = 0;
            this.qty += qty;
        }

        return rejected;
    }

    /**
     * Выдача купюр
     * @param qty - запрашиваемое кол-во купюр
     * @return возвращает выданное кол-во купюр
     */
    private int decQty(int qty){
        int realQty = this.qty >= qty ? qty : this.qty;
        this.qty -= realQty;
        return realQty;
    }

    /**
     * Выдача купюр
     * @param sum - запрашиваемая сумма в рублях
     * @return возвращает сумму недовыданного остатка
     */
    public int decMoney(int sum) {
        return sum - decQty(sum / nominal.getValue()) * nominal.getValue();
    }

    void saveRegistration() {
        qtyOnRegistration = qty;
    }

    void resetToRegistration () {
        qty = qtyOnRegistration;
    }
}
