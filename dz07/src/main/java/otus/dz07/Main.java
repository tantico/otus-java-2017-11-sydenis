package otus.dz07;

public class Main {

    public static void main(String[] args) {
        Department department = new Department();
        Atm atm = new Atm();
        atm.initForTest(2);
        department.registerAtm(atm);

        System.out.println("INIT");
        department.printStates();

        atm.extract(100);
        System.out.println("AFTER EXTRACT");
        department.printStates();

        department.reInitialaize();
        System.out.println("AFTER REINIT");
        department.printStates();

//        Department department = new Department();
//
//        for (int i = 0; i < 10; i++) {
//            Atm atm = new Atm();
//            atm.initForTest(0);
//
//            department.registerAtm(atm);
//        }
//
//        department.getStates();
//
//        department.printStates();
//
//        department.reInitialaize();
    }
}
