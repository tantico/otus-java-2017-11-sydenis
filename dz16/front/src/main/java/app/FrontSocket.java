package app;

import msgsys.common.Front;
import msgsys.messagery.Message;
import msgsys.msocket.ClientSocket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import static msgsys.common.Const.MS_PORT;

public class FrontSocket extends ClientSocket {
    private Front front;
    private static final String HOST = "localhost";
    private static int basicPort = MS_PORT - 1;
    private final int socketId;

    public FrontSocket(Front front) throws IOException {
        super(new Socket(InetAddress.getByName(HOST), MS_PORT, InetAddress.getByName(HOST), --basicPort));
        this.front = front;
        socketId = genSocketId();
    }

    @Override
    public void treatIncomingMsg(Message message) {
        message.execute(front);
    }

    @Override
    public void send(Message message) {
        message.setFrom(socketId);
        super.send(message);
    }

    private int genSocketId() {
        StringBuilder sb = new StringBuilder(getSocket().getLocalPort());
        sb.append(getSocket().getLocalAddress().toString());
        return sb.toString().hashCode();
    }
}
