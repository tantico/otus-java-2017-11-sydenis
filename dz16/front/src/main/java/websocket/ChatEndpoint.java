package websocket;

import app.FrontSocket;
import msgsys.common.Front;
import msgsys.messagery.MessageAuthQuery;
import msgsys.messagery.MessageSave;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/chat/{username}", decoders = MessageDecoder.class, encoders = MessageEncoder.class)
public class ChatEndpoint implements Front {
    private FrontSocket socket;
    private Session session;
    private static final Set<ChatEndpoint> chatEndpoints = new CopyOnWriteArraySet<>();
    private static HashMap<String, String> users = new HashMap<>();

    public ChatEndpoint() throws IOException {
        super();
        socket = new FrontSocket(this);
        socket.init();
    }

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        this.session = session;
        chatEndpoints.add(this);
        users.put(session.getId(), username);

        socket.send(new MessageAuthQuery(username));
    }

    @OnMessage
    public void onMessage(Session session, MessageWS message) throws IOException, EncodeException {
        message.setFrom(users.get(session.getId()));
        broadcast(message);

        socket.send(new MessageSave(message.getFrom(), message.getContent()));
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        chatEndpoints.remove(this);
        MessageWS message = new MessageWS();
        message.setFrom(users.get(session.getId()));
        message.setContent("Disconnected!");
        broadcast(message);
    }

    private static void broadcast(MessageWS message) throws IOException, EncodeException {
        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote()
                        .sendObject(message);
                } catch (IOException | EncodeException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    @Override
    public void handleAuthReply(String reply) {
        MessageWS message = new MessageWS();
        message.setFrom(users.get(session.getId()));

        if (reply != null) {
            message.setContent("Привет, " + reply);
        } else {
            message.setContent("Вас нет в базе! Ваши сообщения не будут сохраняться.");
        }

        try {
            broadcast(message);
        } catch (IOException | EncodeException e) {
            throw new RuntimeException(e);
        }
    }
}
