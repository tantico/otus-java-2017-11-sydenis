package dbservice;

import msgsys.common.DbService;
import msgsys.msocket.ClientSocket;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class DbServiceFirebird implements DbService {
    /**
     * В строке подключения сейчас записан localhost
     * но если дб-сервисы будут располагаться на разных машинах,
     * но обращаться к БД, расположенной на одном хосте,
     * то надо просто прописать тут имя этого хоста
     */
    private static final String DB_HOST = "localhost";

    private ClientSocket socket;

    private Connection dbConn;

    public DbServiceFirebird() throws IOException {
        try {
            DriverManager.registerDriver(new org.firebirdsql.jdbc.FBDriver());

            StringBuilder dbName = new StringBuilder("jdbc:firebirdsql:");
            dbName
                    .append(DB_HOST)
                    .append(":/home/data")
                    .append("/dev/otus-java-2017-11-sydenis/dz16/db/db1.fdb");

            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "masterkey");
            props.setProperty("encoding", "UTF8");

            dbConn = DriverManager.getConnection(dbName.toString(), props);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        socket = new DbSocket(this);
        socket.init();
    }

    public ResultSet execute (String sqlCmd, Object... params) throws SQLException {
        PreparedStatement stmt = dbConn.prepareStatement(sqlCmd);

        if (params != null)
            for (int i = 0; i < params.length; i++)
                stmt.setObject(i + 1, params[i]);

        ResultSet resultSet = stmt.executeQuery();
        return resultSet.next() ? resultSet : null;
    }

    public void CloseDB () throws SQLException {
        dbConn.close();
    }

    @Override
    public String authentificate(String user) {
        String sql = "select name from tab1 where id = ?";
        ResultSet rs = null;
        long id = -1;

        try {
            id = Long.valueOf(user);
            rs = execute(sql, id);
            if (rs != null) {
                sql = rs.getString(1);
                rs.close();
            }
        } catch (NumberFormatException | SQLException n) {
            sql = null;
        }

        return sql;
    }

    @Override
    public void saveMsg(String user, String content){
        String sql = "insert into tab2 (user_id, content) values (?, ?) returning id";
        ResultSet rs = null;
        long id = -1;

        try {
            id = Long.valueOf(user);
            rs = execute(sql, id, content);
            rs.close();
        } catch (NumberFormatException | SQLException n) {
            System.out.println(n.getLocalizedMessage());
        }
    };

    @Override
    public ClientSocket getSocket(){
        return socket;
    };
}
