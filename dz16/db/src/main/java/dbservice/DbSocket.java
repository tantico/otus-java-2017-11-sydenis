package dbservice;

import msgsys.common.DbService;
import msgsys.msocket.ClientSocket;
import msgsys.messagery.Message;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import static msgsys.common.Const.DB_ADDRESS;
import static msgsys.common.Const.MS_PORT;

public class DbSocket extends ClientSocket {
    private DbService dbService;
    private static final String HOST = "localhost";
    private static int basicPort = DB_ADDRESS;

    public DbSocket(DbService dbService) throws IOException {
        super(new Socket( InetAddress.getByName(HOST), MS_PORT, InetAddress.getByName(HOST), ++basicPort));
        this.dbService = dbService;
    }

    @Override
    public void treatIncomingMsg(Message message) {
        message.execute(dbService);
    }

    @Override
    public void send(Message message) {
        message.setFrom(DB_ADDRESS);
        super.send(message);
    }
}
