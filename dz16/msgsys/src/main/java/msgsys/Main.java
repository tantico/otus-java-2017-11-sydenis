package msgsys;

import msgsys.messagery.MessageSystem;
import msgsys.procss.ProcessJAR;
import msgsys.procss.ProcessWAR;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Main {
    private static final String PREFIX = "/home";
    private static final String DEV_PATH = PREFIX + "/data/dev/otus-java-2017-11-sydenis/dz16";

    private static final String DB_START_JAR = "java -jar "+ DEV_PATH + "/db/target/db-1.0-SNAPSHOT.jar";

    private static final String FRONT_SOURCE = DEV_PATH + "/front/target/front-1.0-SNAPSHOT.war";
    private static final String FRONT_DEST = PREFIX + "/data/dev/jetty/webapps/root.war";

    public static void main(String[] args) throws IOException {
        ProcessWAR front = new ProcessWAR(FRONT_SOURCE, FRONT_DEST);

        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.schedule(()->new ProcessJAR(DB_START_JAR), 2, TimeUnit.SECONDS);

        MessageSystem messageSystem = MessageSystem.getInstance();
        messageSystem.start();

//        front.stop();
//        db.stop();
//        messageSystem.stop();
    }
}
