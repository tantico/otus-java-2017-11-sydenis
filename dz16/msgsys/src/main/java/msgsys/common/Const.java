package msgsys.common;

public class Const {
    public static final int MS_PORT = 8888;
    public static final int DB_ADDRESS = MS_PORT + 1;
}
