package msgsys.common;

public interface Front {
    void handleAuthReply(String reply);
}
