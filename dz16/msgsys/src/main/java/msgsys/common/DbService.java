package msgsys.common;

import msgsys.msocket.ClientSocket;

public interface DbService {
    String authentificate(String user);
    void saveMsg(String user, String content);
    ClientSocket getSocket();
}
