package msgsys.msocket;

import com.google.gson.Gson;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import msgsys.messagery.Message;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
/**
 * Исходящие сообщения кладутся в очередь на отправку - output
 * и отправляются оттуда по мере возможности
 * Входящие передаются абстрактной процедуре treatIncomingMsg
 * которая реализует требуемую обработку в каждом из наследников -
 * на фронте, на дб и в ms
 */
public abstract class ClientSocket {
    public static final int WORKERS_COUNT = 2;
    private final LinkedBlockingQueue<Message> output = new LinkedBlockingQueue<>();
    private final ExecutorService executor;

    private final Socket socket;

    public ClientSocket(Socket socket) {
        this.socket = socket;
        this.executor = Executors.newFixedThreadPool(WORKERS_COUNT);
    }

    public void send(Message message) {
        output.add(message);
    }

    public void close() throws IOException {
        executor.shutdown();
        socket.close();
    }

    public void init() {
        executor.execute(this::sendMessage);
        executor.execute(this::receiveMessage);
    }

    private void sendMessage() {
        try (PrintWriter out = new PrintWriter(socket.getOutputStream(), true)) {
            while (socket.isConnected()) {
                Message message = output.take(); //blocks
                String json = new Gson().toJson(message);
                out.println(json);
                out.println();//line with json + an empty line
            }
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void receiveMessage() {
        try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))) {
            String inputLine;
            StringBuilder stringBuilder = new StringBuilder();
            while ((inputLine = in.readLine()) != null) { //blocks
                stringBuilder.append(inputLine);
                if (inputLine.isEmpty()) { //empty line is the end of the message
                    Message message = getMessageFromJSON(stringBuilder.toString());
                    treatIncomingMsg(message);
                    stringBuilder = new StringBuilder();
                }
            }
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static Message getMessageFromJSON(String json) throws ClassNotFoundException {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) jsonParser.parse(json);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String className = (String) jsonObject.get(Message.CLASS_NAME_VARIABLE);
        Class<?> msgClass = Class.forName(className);
        return (Message) new Gson().fromJson(json, msgClass);
    }

    public abstract void treatIncomingMsg(Message message);

    public Socket getSocket() { return socket; }
}



