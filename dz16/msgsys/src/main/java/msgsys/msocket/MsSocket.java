package msgsys.msocket;

import msgsys.messagery.Message;
import msgsys.messagery.MessageSystem;
import java.net.Socket;

public class MsSocket extends ClientSocket {
    private final MessageSystem messageSystem;

    public MsSocket(Socket socket, MessageSystem messageSystem) {
        super(socket);
        this.messageSystem = messageSystem;
    }

    @Override
    public void treatIncomingMsg(Message message) {
        messageSystem.addMsg(message);
    }
}
