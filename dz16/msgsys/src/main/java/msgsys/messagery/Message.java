package msgsys.messagery;

public abstract class Message {
    private static final MessageSystem messageSystem = MessageSystem.getInstance();

    public static final String CLASS_NAME_VARIABLE = "className";
    private final String className;

    private int from;
    private int to;
    /**
     * Отправитель в конструкторе не указывается - его подставляет сокет в момент отправки
     */
    public Message(int to, Class<?> klass) {
        this.className = klass.getName();
        this.to = to;
    }

    public static MessageSystem getMessageSystem() { return messageSystem; }
    public int getFrom() {
        return from;
    }
    public void setFrom(int from) { this.from = from; }
    public int getTo() { return to; }

    public abstract void execute(Object worker);
}
