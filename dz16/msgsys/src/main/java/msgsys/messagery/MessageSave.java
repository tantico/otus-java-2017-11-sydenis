package msgsys.messagery;

import msgsys.common.DbService;
import static msgsys.common.Const.DB_ADDRESS;

public class MessageSave extends Message {
    private String login;
    private String content;

    public MessageSave(String login, String content) {
        super(DB_ADDRESS, MessageSave.class);
        this.login = login;
        this.content = content;
    }

    @Override
    public void execute(Object worker) {
        DbService db = (DbService)worker;
        db.saveMsg(login, content);
    }
}
