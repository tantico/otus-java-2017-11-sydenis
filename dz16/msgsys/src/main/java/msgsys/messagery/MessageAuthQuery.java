package msgsys.messagery;

import msgsys.common.DbService;

import static msgsys.common.Const.DB_ADDRESS;

public class MessageAuthQuery extends Message {
    private String login;

    public MessageAuthQuery(String login) {
        super(DB_ADDRESS, MessageAuthQuery.class);
        this.login = login;
    }

    @Override
    public void execute(Object worker) {
        DbService db = (DbService)worker;
        String reply = db.authentificate(login);

        db.getSocket().send(new MessageAuthReply(getFrom(), reply));
    }
}
