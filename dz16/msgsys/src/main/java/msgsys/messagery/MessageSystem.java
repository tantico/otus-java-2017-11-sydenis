package msgsys.messagery;

import msgsys.msocket.MsSocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.*;

import static msgsys.common.Const.*;
/**
 * Принцип работы MessageSystem
 * =============================
 * 1) Предполагается, что у MS есть только два типа клиентов - фронты и дб-сервисы.
 * У фронтов номер порта меньше, чем у MS, у дб-сервисов - больше.
 * По порту MS определяет какой тип клиента к ней подключается.
 * 2) Соответственно MS поддерживает два отдельных набора сокетов - для фронтов и для дб-сервисов.
 * При подключении нового клиента, MS смотрит на номер удалённого порта и кладёт
 * новорожденный сокет в соответствующий набор.
 * 3) В наборе фронтов есть доп. параметр - уникальный ID каждого фронта, используется для поиска.
 * Идея такова, что при обращении Front>DB не имеет значения через какой дб-сервис
 * лезть в базу - выбирается первый походящий из пула. При обратном обращении DB>Front
 * ответ надо посылать точно тому, кто запрашивал.
 * 4) Доп. инфа - в Message
 */
public class MessageSystem {
    private static final MessageSystem instance = new MessageSystem();

    public static MessageSystem getInstance() {
        return instance;
    }

    private final int THREAD_COUNT = 4;
    private LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();
    private ExecutorService pool;

    private ConcurrentHashMap<Integer, MsSocket> frontClients;
    private CopyOnWriteArrayList<MsSocket> dbClients;

    private MessageSystem() {
        pool = Executors.newFixedThreadPool(THREAD_COUNT);
        frontClients = new ConcurrentHashMap<>();
        dbClients = new CopyOnWriteArrayList<>();
    }

    public void addMsg(Message message) {
        messages.add(message);
    }

    public void start() throws IOException {
        pool.submit(this::processMessages);

        try (ServerSocket serverSocket = new ServerSocket(MS_PORT)) {
            while (!pool.isShutdown()) {
                Socket socket = serverSocket.accept(); //blocks
                MsSocket client = new MsSocket(socket, this);

                if (client.getSocket().getPort() < MS_PORT)  //connection from front
                    frontClients.put(getSocketId(client), client);
                else
                    dbClients.add(client);

                client.init();
            }
        }
    }

    private void processMessages() {
        while (!pool.isShutdown()) {
            try {
                Message message = messages.take();
                int nodeAddress = message.getTo();

                if (nodeAddress == DB_ADDRESS)
                    getRandomDBservice().send(message);
                else
                    frontClients.get(nodeAddress).send(message);

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    public void stop() {
        pool.shutdown();
    }

    /**
     * Выбираем произвольный DBservice - эмуляция пула
     */
    private MsSocket getRandomDBservice() {
        Random r = new Random();
        int dbIndex = r.nextInt(dbClients.size());
        return dbClients.get(dbIndex);
    }

    private int getSocketId(MsSocket remoteSocket) {
        Socket socket = remoteSocket.getSocket();
        StringBuilder sb = new StringBuilder(socket.getPort());
        sb.append(socket.getInetAddress().toString());
        return sb.toString().hashCode();
    }
}
