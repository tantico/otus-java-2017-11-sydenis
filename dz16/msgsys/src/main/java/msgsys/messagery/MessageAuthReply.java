package msgsys.messagery;

import msgsys.common.Front;

import static msgsys.common.Const.DB_ADDRESS;

public class MessageAuthReply extends Message{
    private String reply;

    public MessageAuthReply(int to, String reply) {
        super(to, MessageAuthReply.class);
        this.reply = reply;
    }

    @Override
    public void execute(Object worker) {
        ((Front)worker).handleAuthReply(reply);
    }
}
