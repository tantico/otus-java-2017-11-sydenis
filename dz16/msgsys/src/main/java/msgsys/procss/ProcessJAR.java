package msgsys.procss;

import java.io.IOException;
import java.util.logging.Logger;

public class ProcessJAR {
    private static final Logger logger = Logger.getLogger(ProcessJAR.class.getName());


    private Process process = null;

    public ProcessJAR(String command) {
        try {
            ProcessBuilder pb = new ProcessBuilder(command.split(" "));
            pb.redirectErrorStream(true);
            process = pb.start();

            logger.info("external process DBservice launched");

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void stop() {
        if (process != null)
            process.destroy();
    }
}

