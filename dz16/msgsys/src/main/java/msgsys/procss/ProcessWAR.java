package msgsys.procss;

import java.io.*;
import java.util.logging.Logger;

/**
 * В условии ДЗ было сказано так:
 * если у вас запуск веб приложения в контейнере, то MessageServer может копировать root.war в контейнеры при старте
 *
 * Поэтому здесь просто копирование файла.
 */

public class ProcessWAR {
    private static final Logger logger = Logger.getLogger(ProcessWAR.class.getName());

    final String path;

    public ProcessWAR(String source, String dest) {
        super();

        try {
            copy(source, dest);
            logger.info("external process Front started on Jetty");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.path = dest;
    }

    public void stop() {
        new File(path).delete();
    }

    private void copy(String source, String dest) throws IOException {
        InputStream is = null;
        OutputStream os = null;

        try {
            is = new FileInputStream(source);
            os = new FileOutputStream(dest);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = is.read(buffer)) > 0) {
                os.write(buffer, 0, length);
            }
        } finally {
            is.close();
            os.close();
        }
    }
}
