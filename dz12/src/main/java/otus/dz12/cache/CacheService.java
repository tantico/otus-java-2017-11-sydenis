package otus.dz12.cache;

import otus.dz12.User;

import java.lang.ref.SoftReference;
import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class CacheService{

    public int lifeTime; //ms
    public int maxSize;   //mb
    private final int itemSize = 600; //string[255] + 2*long  ~= 600kb
    private LinkedHashMap<SoftReference<Long>, SoftReference<Item>> data;
    private Timer timer;

    public CacheService(int maxSize, int lifeTime) {
        this.maxSize = maxSize;
        this.lifeTime = lifeTime;
        this.data = new LinkedHashMap<>();
        this.timer = new Timer();
        this.timer.schedule(new LifeCleaner(), this.lifeTime, this.lifeTime);
    }

    private class Item {
        User user;
        Instant
            created,
            lastAccess;
        int hits;

        public Item(User user) {
            this.user = user;
            this.created = Instant.now();
            this.lastAccess = this.created;
            this.hits = 1;
        }

        public void incHits() {
            this.hits++;
            this.lastAccess = Instant.now();
        }
    }

    private class LifeCleaner extends TimerTask{
        @Override
        public void run() {
            cleanUp();
        }
    }

    public void stopCaching () {
        timer.cancel();
        cleanUp();
    }

    public User getItem(long id) {
        SoftReference<Long> key = findKey(id);

        if (key != null) {
            Item item = data.get(key).get();
            item.incHits();

            return item.user;
        }
        else
            return null;
    }

    public void putItem(User user) {
        if (data.size() * itemSize * 1024 > maxSize)
            cleanUp();

        data.put(new SoftReference<Long>(user.getId()), new SoftReference<Item>(new Item(user)));
    }

    private SoftReference<Long> findKey(long id) {
        SoftReference<Long> key = null;

        for (SoftReference<Long> k: data.keySet())
            if (k.get() == id) {
                key = k;
                break;
            }

        return key;
    }

    public void cleanUp() {
//        Iterator<Map.Entry<SoftReference<Long>, SoftReference<Item>>> iterator = data.entrySet().iterator();
//
//        while (iterator.hasNext()) {
//            Map.Entry<SoftReference<Long>, SoftReference<Item>> e = iterator.next();
//
//            if (Duration.between( e.getValue().get().lastAccess, Instant.now()).toMillis() > lifeTime)
//                data.remove(e.getKey());
//        }
    }

    public int objCount() {
        return data.size();
    }

    public int hitCount() {
        int count = 0;

        for (SoftReference<Item> v: data.values())
            count += v.get().hits;

        return count;
    }


}

