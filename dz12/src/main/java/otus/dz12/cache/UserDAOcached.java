package otus.dz12.cache;

import otus.dz12.User;
import otus.dz12.db.DBService;
import otus.dz12.db.UserDAO;
import otus.dz12.db.UserDAOimpl;

import java.sql.SQLException;

public class UserDAOcached implements UserDAO{

    public static final int LIFE_TIME = 700; //ms
    public static final int MAX_SIZE = 10;   //mb

    private UserDAOimpl db;
    private CacheService cache;

    public UserDAOcached() {
        db = new UserDAOimpl();
        cache = new CacheService(MAX_SIZE, LIFE_TIME);
    }

    @Override
    public void connectDB(DBService ds) {
        db.connectDB(ds);
    }

    @Override
    public void disconnectDB() throws SQLException {
        cache.stopCaching();
        db.disconnectDB();
    }

    @Override
    public long insert(User user) throws SQLException {
        long id = db.insert(user);

        if (id != -1)
            cache.putItem(user);

        return id;
    }

    @Override
    public User load(long id) throws SQLException {
        User user = cache.getItem(id);

        if (user == null)
            user = db.load(id);

        return user;
    }


    public int objCount() {
        return cache.objCount();
    }

    public int hitCount() {
        return cache.hitCount();
    }
}
