package otus.dz12;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.server.handler.ResourceHandler;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import otus.dz12.cache.UserDAOcached;
import otus.dz12.db.DBService;
import otus.dz12.db.UserDAO;
import otus.dz12.web.CacheServlet;
import otus.dz12.web.LoginServlet;
import otus.dz12.web.TemplateProcessor;

public class Main {
    public static void main(String[] args) throws Exception {
        UserDAO ud = new UserDAOcached();

        startWeb((UserDAOcached) ud);

        Thread.sleep(5000);

        DBService ds = new DBService();

        ud.connectDB(ds);

        long userID;

        User user = new User();
        user.setName("Donald Trump");
        user.setAge(77);
        userID = ud.insert(user);

        System.out.println("just added:");
        System.out.println(ud.load(userID).toString());

        System.out.println();
        System.out.println("waiting for autocleaning cache...");

        for (int i = 0; i < 3; i++) {
            Thread.sleep(1000);
            System.out.println(ud.load(userID).toString());
        }

        ud.disconnectDB();
    }

    private static void startWeb(UserDAOcached ud) throws Exception {
        final int PORT = 8090;

        ResourceHandler resourceHandler = new ResourceHandler();
        resourceHandler.setResourceBase(TemplateProcessor.HTML_DIR);

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(new LoginServlet("anonymous")), "/login");
        context.addServlet(new ServletHolder(new CacheServlet(ud)), "/cache");

        Server server = new Server(PORT);
        server.setHandler(new HandlerList(resourceHandler, context));

        server.start();
//        server.join();
    }
}
