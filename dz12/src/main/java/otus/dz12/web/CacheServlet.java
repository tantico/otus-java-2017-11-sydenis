package otus.dz12.web;

import otus.dz12.cache.UserDAOcached;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CacheServlet extends HttpServlet {

    private static final String CACHE_HTML = "cache.html";
    private UserDAOcached cache;

    public CacheServlet(UserDAOcached cache) {
        this.cache = cache;
    }

    private static Map<String, Object> createPageVariablesMap(HttpServletRequest request) {
        Map<String, Object> pageVariables = new HashMap<>();
        pageVariables.put("method", request.getMethod());
        pageVariables.put("URL", request.getRequestURL().toString());
        pageVariables.put("locale", request.getLocale());
        pageVariables.put("sessionId", request.getSession().getId());
        pageVariables.put("parameters", request.getParameterMap().toString());

        return pageVariables;
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response) throws ServletException, IOException {

        Map<String, Object> pageVariables = createPageVariablesMap(request);

        pageVariables.put("size", cache.MAX_SIZE);
        pageVariables.put("count", cache.objCount());
        pageVariables.put("hits", cache.hitCount());
        pageVariables.put("life", cache.LIFE_TIME);

        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println(TemplateProcessor.instance().getPage(CACHE_HTML, pageVariables));
    }
}
