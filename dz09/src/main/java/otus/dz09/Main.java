package otus.dz09;

import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException {
        DBService ds = new DBService();

        UserDAO ud = new UserDAOimpl();
        ud.connectDB(ds);

        long existingUser = 2;

        User user = ud.load(existingUser);
        System.out.println("existing:");
        System.out.println(user.toString());

        user = new User();
        user.setName("Donald Trump");
        user.setAge(77);
        existingUser = ud.insert(user);

        user = null;

        user = ud.load(existingUser);
        System.out.println("just added:");
        System.out.println(user.toString());

        ud.disconnectDB();
    }
}
