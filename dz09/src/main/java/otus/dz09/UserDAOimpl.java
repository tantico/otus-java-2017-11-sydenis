package otus.dz09;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAOimpl implements UserDAO {
    private DBService ds;

    @Override
    public void connectDB(DBService ds) {
        this.ds = ds == null ? new DBService() : ds;
    }

    @Override
    public void disconnectDB() throws SQLException {
        ds.CloseDB();
    }

    @Override
    public long insert(User user)  throws SQLException  {
        String sqlCmd = "insert into tab1 (name, age) values (?, ?) returning id";
        ResultSet rs = ds.execute(sqlCmd, user.getName(), user.getAge());

        if (rs == null) return -1;

        user.setId(rs.getLong(1));
        rs.close();

        return user.getId();
    }

    @Override
    public User load(long id) throws SQLException {
        User user;

        String sqlCmd = "select name, age from tab1 where id = ?";
        ResultSet rs = ds.execute(sqlCmd, id);

        if (rs != null) {
            user = new User();
            user.setId(id);
            user.setName(rs.getString(1));
            user.setAge(rs.getInt(2));
            rs.close();
        } else
            user = null;

        return user;
    }
}
