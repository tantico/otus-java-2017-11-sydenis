package otus.dz09;

import java.sql.SQLException;

public interface UserDAO {
    void connectDB(DBService ds);
    void disconnectDB() throws SQLException;
    long insert(User user) throws SQLException;
    User load(long id) throws SQLException;
}
