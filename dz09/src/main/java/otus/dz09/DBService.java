package otus.dz09;

import java.sql.*;
import java.util.Properties;

public class DBService {
    private Connection dbConn;

    public DBService() {
        try {
            DriverManager.registerDriver(new org.firebirdsql.jdbc.FBDriver());
            String
                    url = "jdbc:firebirdsql:localhost:",
                    dbname ="/home/data/dev/otus-java-2017-11-sydenis/dz09/db1.fdb";

            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "masterkey");
            props.setProperty("encoding", "UTF8");

            dbConn = DriverManager.getConnection(url + dbname, props);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ResultSet execute (String sqlCmd, Object... params) throws SQLException {
        PreparedStatement stmt = dbConn.prepareStatement(sqlCmd);

        for (int i = 0; i < params.length; i++)
            stmt.setObject(i + 1, params[i]);

        ResultSet resultSet = stmt.executeQuery();
        return resultSet.next() ? resultSet : null;
    }

    public void CloseDB () throws SQLException {
        dbConn.close();
    }
}
