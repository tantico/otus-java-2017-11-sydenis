package otus.dz15.db;

public interface DbService {
    String authentificate(String user);
    void saveMsg(String user, String content);
}
