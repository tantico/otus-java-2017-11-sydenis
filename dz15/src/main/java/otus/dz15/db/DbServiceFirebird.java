package otus.dz15.db;

import java.sql.*;
import java.util.Properties;

public class DbServiceFirebird implements DbService {
    private static final DbServiceFirebird instance = new DbServiceFirebird();

    public static DbServiceFirebird getInstance() {
        return instance;
    }

    private Connection dbConn;

    private DbServiceFirebird() {
        try {
            DriverManager.registerDriver(new org.firebirdsql.jdbc.FBDriver());
            String
                    url = "jdbc:firebirdsql:localhost:",
                    dbname ="/home/data/dev/otus-java-2017-11-sydenis/dz15/db1.fdb";

            Properties props = new Properties();
            props.setProperty("user", "SYSDBA");
            props.setProperty("password", "masterkey");
            props.setProperty("encoding", "UTF8");

            dbConn = DriverManager.getConnection(url + dbname, props);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public ResultSet execute (String sqlCmd, Object... params) throws SQLException {
        PreparedStatement stmt = dbConn.prepareStatement(sqlCmd);

        if (params != null)
            for (int i = 0; i < params.length; i++)
                stmt.setObject(i + 1, params[i]);

        ResultSet resultSet = stmt.executeQuery();
        return resultSet.next() ? resultSet : null;
    }

    public void CloseDB () throws SQLException {
        dbConn.close();
    }

    @Override
    public String authentificate(String user) {
        String sql = "select name from tab1 where id = ?";
        ResultSet rs = null;
        long id = -1;

        try {
            id = Long.valueOf(user);
            rs = execute(sql, id);
            if (rs != null) {
                sql = rs.getString(1);
                rs.close();
            }
        } catch (NumberFormatException | SQLException n) {
            sql = null;
        }

        return sql;
    }

    @Override
    public void saveMsg(String user, String content){
        String sql = "insert into tab2 (user_id, content) values (?, ?) returning id";
        ResultSet rs = null;
        long id = -1;

        try {
            id = Long.valueOf(user);
            rs = execute(sql, id, content);
            rs.close();
        } catch (NumberFormatException | SQLException n) {
            System.out.println(n.getLocalizedMessage());
        }
    };
}
