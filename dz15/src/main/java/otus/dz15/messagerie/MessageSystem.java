package otus.dz15.messagerie;

import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MessageSystem {
    private final int THREAD_COUNT = 4;
    private Thread[] workers = new Thread[THREAD_COUNT];
    private Thread mainThread;

    private MessageSystem()  {
          mainThread = new Thread(()->instance.start());
          mainThread.start();
    }

    private static final MessageSystem instance = new MessageSystem();

    public static MessageSystem getInstance() {
        return instance;
    }

    private BlockingQueue<Message> messages = new LinkedBlockingQueue<>();

    public void addMsg(Message message) {
        messages.add(message);
    }

    private void start() {
        int i = 0;

        while (!Thread.currentThread().isInterrupted()) {
            if (workers[i] == null || workers[i].getState() == Thread.State.TERMINATED) {

                workers[i] = new Thread(() -> {
                    try {
                        messages.take().execute();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });

                workers[i].start();
            }

            if (i < THREAD_COUNT - 1)
                i++;
            else
                i = 0;

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void stop() {
        mainThread.interrupt();
    }
}
