package otus.dz15.messagerie;

import otus.dz15.db.DbServiceFirebird;
import otus.dz15.websocket.ChatEndpoint;

public class MessageAuthReply extends Message{
    private String reply;

    public MessageAuthReply(Object to, String reply) {
        super(DbServiceFirebird.getInstance(), to);
        this.reply = reply;
    }

    @Override
    public void execute() {
        ChatEndpoint chat = (ChatEndpoint)getTo();
        chat.handleAuthReply(reply);
    }
}
