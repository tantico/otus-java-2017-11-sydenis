package otus.dz15.messagerie;

import otus.dz15.db.DbService;
import otus.dz15.db.DbServiceFirebird;

public class MessageAuthQuery extends Message{
    private String login;

    public MessageAuthQuery(Object from, String login) {
        super(from, DbServiceFirebird.getInstance());
        this.login = login;
    }

    @Override
    public void execute() {
        DbService db = (DbService)getTo();
        String reply = db.authentificate(login);

        new MessageAuthReply(getFrom(), reply).send();
    }
}
