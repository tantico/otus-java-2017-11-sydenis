package otus.dz15.messagerie;

import otus.dz15.db.DbService;
import otus.dz15.db.DbServiceFirebird;

public class MessageSave extends Message {
    private String login;
    private String content;

    public MessageSave(Object from, String login, String content) {
        super(from, DbServiceFirebird.getInstance());
        this.login = login;
        this.content = content;
    }

    @Override
    public void execute() {
        DbService db = (DbService)getTo();
        db.saveMsg(login, content);
    }
}
