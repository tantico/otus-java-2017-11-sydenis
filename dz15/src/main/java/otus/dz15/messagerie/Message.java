package otus.dz15.messagerie;

public abstract class Message {
    private static final MessageSystem messageSystem = MessageSystem.getInstance();

    private Object from;
    private Object to;

    public Message(Object from, Object to) {
        this.from = from;
        this.to = to;
    }

    public static MessageSystem getMessageSystem() { return messageSystem; }
    public Object getFrom() {
        return from;
    }
    public Object getTo() {
        return to;
    }

    public abstract void execute();

    public void send() {
        messageSystem.addMsg(this);
    };
}
