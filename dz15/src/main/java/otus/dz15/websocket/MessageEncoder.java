package otus.dz15.websocket;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.google.gson.Gson;
import otus.dz15.messagerie.MessageWS;

public class MessageEncoder implements Encoder.Text<MessageWS> {

    private static Gson gson = new Gson();

    @Override
    public String encode(MessageWS message) throws EncodeException {
        String json = gson.toJson(message);
        return json;
    }

    @Override
    public void init(EndpointConfig endpointConfig) {
        // Custom initialization logic
    }

    @Override
    public void destroy() {
        // Close resources
    }
}
