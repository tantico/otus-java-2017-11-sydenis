package otus.dz15.websocket;

import otus.dz15.messagerie.MessageAuthQuery;
import otus.dz15.messagerie.MessageSave;
import otus.dz15.messagerie.MessageWS;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint(value = "/chat/{username}", decoders = MessageDecoder.class, encoders = MessageEncoder.class)
public class ChatEndpoint {
    private Session session;
    private static final Set<ChatEndpoint> chatEndpoints = new CopyOnWriteArraySet<>();
    private static HashMap<String, String> users = new HashMap<>();

    @OnOpen
    public void onOpen(Session session, @PathParam("username") String username) {
        this.session = session;
        chatEndpoints.add(this);
        users.put(session.getId(), username);

        new MessageAuthQuery(this, username).send();
    }

    @OnMessage
    public void onMessage(Session session, MessageWS message) throws IOException, EncodeException {
        message.setFrom(users.get(session.getId()));
        broadcast(message);

        new MessageSave(this, message.getFrom(), message.getContent()).send();
    }

    @OnClose
    public void onClose(Session session) throws IOException, EncodeException {
        chatEndpoints.remove(this);
        MessageWS message = new MessageWS();
        message.setFrom(users.get(session.getId()));
        message.setContent("Disconnected!");
        broadcast(message);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
        // Do error handling here
    }

    private static void broadcast(MessageWS message) throws IOException, EncodeException {
        chatEndpoints.forEach(endpoint -> {
            synchronized (endpoint) {
                try {
                    endpoint.session.getBasicRemote()
                        .sendObject(message);
                } catch (IOException | EncodeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void handleAuthReply(String reply) {
        MessageWS message = new MessageWS();
        message.setFrom(users.get(session.getId()));

        if (reply != null) {
            message.setContent("Привет, " + reply);
        } else {
            message.setContent("Вас нет в базе! Ваши сообщения не будут сохраняться.");
        }

        try {
            broadcast(message);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (EncodeException e) {
            e.printStackTrace();
        }
    }
}
