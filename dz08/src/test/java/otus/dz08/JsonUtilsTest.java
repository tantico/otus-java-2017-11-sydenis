package otus.dz08;

import com.google.gson.Gson;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class JsonUtilsTest {

    @Test
    void IsRestorable () throws IllegalAccessException {
        ClassA testedObject = new ClassA();
        String js = JsonUtils.save(testedObject);

        Gson gson = new Gson();
        ClassA restoredObject = gson.fromJson(js, ClassA.class);

        assertTrue(testedObject.equals(restoredObject));
    }
}