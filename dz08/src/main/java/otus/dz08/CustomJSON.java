package otus.dz08;

import java.util.ArrayList;
import java.util.HashMap;

public class CustomJSON {
    public interface Exportable{
        public String toJSONString();
    }

    public static class JSONObject implements Exportable {
        private HashMap<Object, Object> data = new HashMap<>();

        public void put(Object key, Object value){
            data.put(key, value);
        }


        public String toJSONString() {
            StringBuilder sb = new StringBuilder("{");

            for (HashMap.Entry<Object, Object> rec: data.entrySet()){
                sb.append("\"");
                sb.append(rec.getKey());
                sb.append("\":");
                sb.append(ajustItem(rec.getValue()));
                sb.append(",");
            }

            sb.deleteCharAt(sb.length() - 1);
            return sb.append("}").toString();
        }
    }

    public static class JSONArray extends ArrayList implements Exportable {
        public String toJSONString() {
            StringBuilder sb = new StringBuilder("[");

            for (Object item: this){
                sb.append(ajustItem(item));
                sb.append(",");
            }

            sb.deleteCharAt(sb.length() - 1);
            return sb.append("]").toString();
        }
    }

    private static String ajustItem(Object item) {
        StringBuilder sb = new StringBuilder();

        if (item instanceof Exportable)
            sb.append(((Exportable) item).toJSONString());

        else if (item instanceof String) {
            sb.append("\"");
            sb.append(item);
            sb.append("\"");
        } else
            sb.append(item);

        return sb.toString();
    }
}
