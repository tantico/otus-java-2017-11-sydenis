package otus.dz08;

import java.util.ArrayList;
import java.util.Arrays;

public class ClassA {
    private int intField = 5;
    public boolean boolField = true;
    private String stringField = "ABC";
    private short[] arrayField = {1, 2, 3, 4, 5};
    private ArrayList<String> listField;

    public ClassA() {
        this.listField = new ArrayList<>();
        listField.add("aaa");
        listField.add("bbb");
        listField.add("ccc");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        else if (o == null)
            return false;
        else if (getClass() != o.getClass())
            return  false;

        ClassA that = (ClassA) o;

        if ((this.intField != that.intField) ||
            (this.boolField != that.boolField) ||
             !this.stringField.equals(that.stringField) ||
             !this.listField.equals(that.listField) ||
             !Arrays.equals(this.arrayField, that.arrayField))

            return false;
        else
            return true;
    }

}
