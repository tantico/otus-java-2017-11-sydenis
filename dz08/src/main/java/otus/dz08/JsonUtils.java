package otus.dz08;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import otus.dz08.CustomJSON.JSONObject;
import otus.dz08.CustomJSON.JSONArray;

import static java.lang.reflect.Array.get;
import static java.lang.reflect.Array.getLength;

public class JsonUtils {
    public static String save(Object o) throws IllegalAccessException {
        return objToJSON(o).toJSONString();
    }

    private static JSONObject objToJSON(Object o) throws IllegalAccessException {
        JSONObject root = new JSONObject();
        Field[] fields = o.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            String currentName = fields[i].getName();
            Object currentVal = fields[i].get(o);

            switch (router(fields[i].getType())) {
                case Map:
                    currentVal = leafMap((HashMap)currentVal);
                    break;
                case List:
                    currentVal = leafList((Iterable)currentVal);
                    break;
                case Array:
                    currentVal = leafArray(currentVal);
                    break;
                case Object:
                    currentVal = leafObject(currentVal);
                    break;
            }

            root.put(currentName, currentVal);
        }
        return root;
    }

    private static JSONObject leafObject(Object o) throws IllegalAccessException {
        JSONObject leaf = new JSONObject();
        Field[] fields = o.getClass().getDeclaredFields();

        for (int i = 0; i < fields.length; i++) {
            fields[i].setAccessible(true);
            String currentName = fields[i].getName();
            Object currentVal = fields[i].get(o);
            leaf.put(currentName, isComplex(currentVal) ? objToJSON(currentVal) : currentVal);
        }

        return leaf;
    }

    private static JSONObject leafMap(HashMap map) throws IllegalAccessException {
        JSONObject leaf = new JSONObject();

        for (Object key : map.keySet())
            leaf.put(key.toString(), objToJSON(map.get(key)));

        return leaf;
    }

    private static JSONArray leafList(Iterable list) throws IllegalAccessException {
        JSONArray leaf = new JSONArray();

        for (Object item: list)
            leaf.add(isComplex(item) ? objToJSON(item) : item);

        return leaf;
    }

    private static JSONArray leafArray(Object array) throws IllegalAccessException {
        JSONArray leaf = new JSONArray();

        for (int i = 0; i < getLength(array); i++) {
            Object item = get(array, i);
            leaf.add(isComplex(item) ? objToJSON(item) : item);
        }

        return leaf;
    }

    private static FieldCat router(Class fieldClass) {
        if (fieldClass.isPrimitive() || (fieldClass == String.class) || (fieldClass.getSuperclass() == Number.class))
            return FieldCat.Simple;
        else if (Arrays.asList(fieldClass.getInterfaces()).contains(Map.class))
            return FieldCat.Map;
        else if (Arrays.asList(fieldClass.getInterfaces()).contains(List.class))
            return FieldCat.List;
        else if (fieldClass.getComponentType() != null)
            return FieldCat.Array;
        else
            return FieldCat.Object;
    }

    private static boolean isComplex(Object o) {
        return router(o.getClass()) != FieldCat.Simple;
    }

    private static enum FieldCat{
        Simple, Map, List, Array, Object
    }
}
