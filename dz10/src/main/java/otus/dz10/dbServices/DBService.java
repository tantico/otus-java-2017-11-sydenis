package otus.dz10.dbServices;

import otus.dz10.dataSets.UserDataSet;

import java.sql.SQLException;

public interface DBService {
    void save(UserDataSet user);
    UserDataSet loadByName(String name);
    void disconnectDB();
}
