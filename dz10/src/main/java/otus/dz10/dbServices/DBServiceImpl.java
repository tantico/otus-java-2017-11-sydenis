package otus.dz10.dbServices;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.hibernate.service.ServiceRegistry;

import otus.dz10.dataSets.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

public class DBServiceImpl implements DBService {
    private final SessionFactory sessionFactory;

    public DBServiceImpl() {
        String dbpath = "/home/data/dev/otus-java-2017-11-sydenis/dz10/db1.fdb";

        Configuration configuration = new Configuration();

        configuration.addAnnotatedClass(UserDataSet.class);
        configuration.addAnnotatedClass(AddressDataSet.class);

        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.FirebirdDialect");
        configuration.setProperty("hibernate.connection.driver_class", "org.firebirdsql.jdbc.FBDriver");
        configuration.setProperty("hibernate.connection.url", "jdbc:firebirdsql:localhost:" + dbpath);
        configuration.setProperty("hibernate.connection.username", "sysdba");
        configuration.setProperty("hibernate.connection.password", "masterkey");
        configuration.setProperty("hibernate.connection.encoding","UTF8");
        configuration.setProperty("hibernate.show_sql", "true");
        configuration.setProperty("hibernate.hbm2ddl.auto", "validate");

        sessionFactory = createSessionFactory(configuration);
    }

    public DBServiceImpl(Configuration configuration) {
        sessionFactory = createSessionFactory(configuration);
    }

    private static SessionFactory createSessionFactory(Configuration configuration) {
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        builder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = builder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    @Override
    public void save(UserDataSet dataSet) {
        try (Session session = sessionFactory.openSession()) {
            session.save(dataSet);
        }
    }

    @Override
    public UserDataSet loadByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<UserDataSet> criteria = builder.createQuery(UserDataSet.class);
            Root<UserDataSet> from = criteria.from(UserDataSet.class);
            criteria.where(builder.equal(from.get("name"), name));
            Query<UserDataSet> query = session.createQuery(criteria);
            return query.uniqueResult();
        }
    }

    @Override
    public void disconnectDB() {
        sessionFactory.close();
    }
}
