package otus.dz10.dataSets;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class UserDataSet extends DataSet{
    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int age;
//    @Column(name = "address_id")
    @OneToOne(cascade = CascadeType.ALL)
    private  AddressDataSet address;

    public UserDataSet() {}

    @Override
    public String toString() {
        return "User id = " + getId() + ", name = " + getName() + ", age = " + getAge() + ", address = " + getAddress().toString();
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public AddressDataSet getAddress() {
        return address;
    }

    public void setAddress(AddressDataSet address) {
        this.address = address;
    }
}
