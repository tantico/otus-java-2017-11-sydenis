package otus.dz10;

import otus.dz10.dataSets.AddressDataSet;
import otus.dz10.dataSets.UserDataSet;
import otus.dz10.dbServices.DBService;
import otus.dz10.dbServices.DBServiceImpl;

public class Main {
    public static void main(String[] args) {
        DBService dbService = new DBServiceImpl();

        UserDataSet user = new UserDataSet();
        user.setName("Donald Trump");
        user.setAge(77);

        AddressDataSet address = new AddressDataSet();
        address.setStreet("Trump tower");

        user.setAddress(address);

        dbService.save(user);
        user = null;

        user = dbService.loadByName("Donald Trump");
        System.out.println(user);

        dbService.disconnectDB();
    }
}

