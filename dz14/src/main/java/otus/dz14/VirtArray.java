package otus.dz14;

import java.util.Arrays;

public class VirtArray {
    private int [] baseArray;
    private int
        currIndex,
        toIndex;
    public boolean isSorted = false;

    public VirtArray(int[] baseArray, int fromIndex, int toIndex) {
        this.baseArray = baseArray;
        currIndex = fromIndex;
        this.toIndex = toIndex;
    }

    public int getCurrent() {
        return currIndex >= toIndex ? Integer.MAX_VALUE : baseArray[currIndex];
    }

    public void next() {
        currIndex++;
    }

    public void sort() {
        (new Thread(() -> {
            Arrays.sort(baseArray, currIndex, toIndex);
            isSorted = true;
        })).start();
    }
}
