package otus.dz14;

public class Sorter {
    private int [] baseArray;
    private int threadCount = 1;
    private VirtArray [] vArrays;

    public void setArray(int[] array) {
        this.baseArray = array;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
        this.vArrays = new VirtArray[threadCount];
    }

    private int[] getBorders() {
        int[] borders = new int[threadCount + 1];

        int bord = baseArray.length / threadCount;

        borders[0] = 0;
        int i = 1;

        while (i < threadCount) {
            borders[i] = i * bord;
            i++;
        }

        borders[i] = baseArray.length;

        return borders;
    }

    private void sortInThreads() {
        int[] borders = getBorders();;

        for (int i = 0; i < threadCount; i++) {
            vArrays[i] = new VirtArray(baseArray, borders[i], borders[i+1]);
            vArrays[i].sort();
        }
    }

    private int min() {
        int vArrayIndex = 0;
        int min = vArrays[vArrayIndex].getCurrent();

        for (int i = 1; i < threadCount; i++) {
            int value = vArrays[i].getCurrent();

            if (value < min) {
                min = value;
                vArrayIndex = i;
            }
        }

        vArrays[vArrayIndex].next();
        return min;
    }

    private int[] merge() {
        int [] result = new int[baseArray.length];

        for (int i = 0; i < result.length; i++)
            result[i] = min();

        return result;
    }

    public int[] sort() {
        sortInThreads();
        waitForSorting();

        baseArray = merge();
        return baseArray;
    }

    private void waitForSorting() {
        while (true) {
            boolean done = vArrays[0].isSorted;

            for (int i = 1; i < threadCount; i++)
                done = done && vArrays[i].isSorted;

            if (done) break;
        }
    }
}
