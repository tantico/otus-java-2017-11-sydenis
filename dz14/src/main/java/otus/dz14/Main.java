package otus.dz14;

import java.util.Random;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int MAXVAL = 20;
        int THREADS = 4;
        Random rnd = new Random();
        int[] testArray = new int[MAXVAL];

        for (int i = 0; i < MAXVAL; i++)
            testArray[i] = rnd.nextInt(MAXVAL*10);

        prn("Исходный массив", testArray);

        Sorter s = new Sorter();
        s.setThreadCount(THREADS);
        s.setArray(testArray);

        int [] goodResult = s.sort();
        prn("Отсортированный массив", goodResult);

        prn("Проблемма со ссылкой на массив:", testArray);
    }

    public static void prn(String cap, int [] array) {
        System.out.println("==== " + cap + " =====");
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        System.out.println();
        System.out.println();
    }
}
