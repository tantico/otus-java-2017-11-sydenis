package otus.dz03;

import java.util.*;
import java.util.function.Consumer;

public class CustArr<T> implements List<T> {
    private void goException (String name) {
        throw new RuntimeException("*** TEST EXCEPTION : " + name);
    };

    private Object[] data;
    private int defCapa = 10;
    private int lastPosition = -1;

    public CustArr () {
        super();
        data = new Object[defCapa];
    };

    public CustArr (int capa) {
        super();
        data = new Object[capa];
    };


    @Override
    public int size() {
        return lastPosition + 1;
    }

    @Override
    public boolean isEmpty() {
        return lastPosition == -1;
    }

    @Override
    public boolean contains(Object o) {
        goException("contains");
        return false;
    }

    @Override
    public Iterator<T> iterator() {
         return listIterator();
    }

    @Override
    public Object[] toArray() {
        if (size() > 0)
            return Arrays.copyOfRange(data, 0, lastPosition);
        else
            return null;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        goException("toArray(T[] a)");
        return null;
    }

    @Override
    public boolean add(T t) {
        if (lastPosition == data.length - 1)
            data = Arrays.copyOf(data, data.length + defCapa);

        data[++lastPosition] = t;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int index = -1;

        for (int i = 0; i <= lastPosition ; i++)
            if (data[i].equals(o)) {
                index = i;
                break;
            }

        if (index == -1)
            return false;

        for (int i = index; i < lastPosition ; i++) {
            data[i] = data[i + 1];
        }

        data[lastPosition--] = null;
        return true;
    }


    public void UpdateSize() {
        data = toArray();
    }


    @Override
    public boolean containsAll(Collection<?> c) {
        goException("containsAll");
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        Iterator it = c.iterator();

        if (it.hasNext()) {
            while (it.hasNext())
                this.add((T)it.next());

            return true;
        }
        else
            return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        goException("addAll(int index, Collection<? extends T> c)");
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        goException("removeAll");
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        goException("retainAll");
        return false;
    }

    @Override
    public void clear() {
        goException("clear");
    }

    @Override
    public T get(int index) {
        return (T)data[index];
    }

    @Override
    public T set(int index, T element) {
        T old = get(index);

        data[index] = element;

        return old;
    }

    @Override
    public void add(int index, T element) {
        goException("add(int index, T element)");
    }

    @Override
    public T remove(int index) {
        goException("remove");
        return null;
    }

    @Override
    public int indexOf(Object o) {
        goException("indexOf");
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        goException("lastIndexOf");
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return new CustIterator();
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        goException("listIterator(int index)");
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        goException("subList");
        return null;
    }



    // Iterator
    private class CustIterator implements ListIterator {

        private int index = 0;

        @Override
        public boolean hasNext() {
            return index < data.length;
        }

        @Override
        public Object next() {
            return data[index++];
        }

        @Override
        public boolean hasPrevious() {
            return index > 0;
        }

        @Override
        public Object previous() {
            return data[index--];
        }

        @Override
        public int nextIndex() {
            return index + 1;
        }

        @Override
        public int previousIndex() {
            return index - 1;
        }

        @Override
        public void remove() {
            goException("ITERATOR remove");
        }

        @Override
        public void set(Object o) {
            data[index - 1] = o;
        }

        @Override
        public void add(Object o) {
            goException("ITERATOR add(Object o)");
        }

        @Override
        public void forEachRemaining(Consumer action) {
            goException("ITERATOR forEachRemaining");
        }
    }



    //  Suplementary
    public void printData(String name) {
        System.out.print(name + ": ");
        for (int i = 0; i < data.length; i++)
            System.out.print(data[i] + " ");
        System.out.println();
    }
}
