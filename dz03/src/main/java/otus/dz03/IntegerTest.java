package otus.dz03;

import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

public class IntegerTest {
    private int elemCount;
    private CustArr<Integer> testArray = new CustArr();

    static {
        System.out.println("* * * Tests on <T> = Integer * * *");
    }

    public IntegerTest(int elemCount) {
        this.elemCount = elemCount;
    }

    public void testAddAll() {
        Random rn = new Random();
        Integer[] src = new Integer[elemCount];

        for (int i = 0; i < elemCount; i++)
            src[i] = rn.nextInt(99999);

        Collections.addAll(testArray, src);

        System.out.println("results for Collections.addAll:");
        System.out.print("src:  ");
        for (int i: src) System.out.print(i + " ");
        System.out.println();
        testArray.printData("dest");
    }

    public void testCopy () {
        CustArr<Integer> dest = new CustArr<>();
        for (int i = 0; i < elemCount; i++)
            dest.add(0);

        Collections.copy(dest, testArray);

        System.out.println("results for Collections.copy:");
        testArray.printData("src ");
        dest.printData("dest");
    }

    public void testSort() {
        class MyComparator implements Comparator<Integer> {
            public int compare(Integer a, Integer b){
                return a > b ? 1 : -1;
            }
        }

        MyComparator c = new MyComparator();
        Collections.sort(testArray, c);
        System.out.println("results for Collections.sort:");
        testArray.printData("sorted");
    }

    public void goAllTests(){
        testAddAll();
        testCopy();
        testSort();
    }
}
