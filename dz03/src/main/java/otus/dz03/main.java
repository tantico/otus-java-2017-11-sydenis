package otus.dz03;

public class main {
    public static void main(String[] args) {
        int elemCount = 10;

        IntegerTest it = new IntegerTest(elemCount);
        it.goAllTests();

        StringTest st = new StringTest(elemCount);
        st.goAllTests();
    }
}
