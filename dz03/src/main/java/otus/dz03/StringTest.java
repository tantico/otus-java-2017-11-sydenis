package otus.dz03;

import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import static java.lang.Math.abs;

public class StringTest {
    private int elemCount;
    private CustArr<String> testArray = new CustArr();

    static {
        System.out.println("* * * Tests on <T> = String * * *");
    }

    public StringTest(int elemCount) {
        this.elemCount = elemCount;
    }

    public void testAddAll() {
        Random rn = new Random();
        String[] src = new String[elemCount];
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < elemCount; i++) {
            sb.setLength(0);
            for (int j = 0; j < 8; j++) sb.append((char) (100 + abs(rn.nextInt(150))));
            src[i] = sb.toString();
        }

        Collections.addAll(testArray, src);

        System.out.println("results for Collections.addAll:");
        System.out.print("src:  ");
        for (String i: src) System.out.print(i + " ");
        System.out.println();
        testArray.printData("dest");
    }

    public void testCopy () {
        CustArr<String> dest = new CustArr<>();
        for (int i = 0; i < elemCount; i++)
            dest.add("0");

        Collections.copy(dest, testArray);

        System.out.println("results for Collections.copy:");
        testArray.printData("src ");
        dest.printData("dest");
    }

    public void testSort() {
        class MyComparator implements Comparator<String> {
            public int compare(String a, String b){
                return a.compareTo(b);
            }
        }

        MyComparator c = new MyComparator();
        Collections.sort(testArray, c);
        System.out.println("results for Collections.sort:");
        testArray.printData("sorted");
    }

    public void goAllTests(){
        testAddAll();
        testCopy();
        testSort();
    }
}
